/*
 * Copyright 2022 Flexiv Inc.
 * 
 * Demo of link ee deflection due to link and joint flexibility at different poses
 * 
 * @author: Houzhu Ding (houzhu.ding@flexiv.com) 
 *
 */

#include <iostream>
#include <vector>
#include <string>
#include <Eigen/Dense>
#include <tuple>
#include <kinematics_flex.h>
#include <thirdparty/matplotlibcpp.h>

using namespace Eigen;
namespace plt = matplotlibcpp;

int main() {
    std::vector<std::string> msg {"A02L Robotic Arm Positioning Accuracy Improvement:","Flexible Link/Joint Modeling"};

    for (const std::string &word : msg) 
    {
      std::cout << word << " ";
    }
    std::cout << std::endl;
    // Some common pose of robot

    // std::vector<double> q_deg = {0,-40,0,90,0,130,0}; // home
    // std::vector<double> q_deg = {0,-90,0,0,0,90,0}; // horizontal
    // std::vector<double> q_deg = {0,0,0,0,0,0,0}; // vertical
    // std::vector<double> q_deg = {0,-90,-90,-90,-90,-90,0};

    // Define the payload info
    double input_payload = 4; //kg
    std::vector<double> input_payload_com = {0,0,0}; //meter

    // Define the pose of the robot
    int pt_num = 5000;

    std::vector<double> q_deg = {0,-40,0,90,0,130,0}; // home
    // Define the velocity and acceleration of robot
    std::vector<double> qdot = {0,0,0,0,0,0,0,0};
    std::vector<double> qddot = {0,0,0,0,0,0,0,0};
    // Define output matrix for error
    Vector3d links_error, joints_error, total_error;
    MatrixXd defm_results = MatrixXd(9,pt_num); 
    std::vector<double> error_x_total,error_y_total,error_z_total;
    for(int i=0;i< pt_num;i++)
    {
      MatrixXd q_deg_mat = MatrixXd::Random(1,7)*180;//define random joint pose between -180-+180
      // std::cout<<q_deg_mat<<std::endl;
      Map<MatrixXd>(q_deg.data(),q_deg_mat.rows(),q_deg_mat.cols()) = q_deg_mat;
      std::vector<double> q_rad = kinematics_basic::deg2rad(q_deg);
      
      std::tie(links_error,joints_error) = kinematics_flex::getT2BaseFlexible(q_rad,qdot,qddot,input_payload,input_payload_com);
      total_error = links_error+joints_error;
      defm_results(seqN(0,3),seqN(i,1)) = links_error ; // link error
      defm_results(seqN(3,3),seqN(i,1)) = joints_error; // joint error
      defm_results(seqN(6,3),seqN(i,1)) = total_error; // total error
      error_x_total.push_back(total_error(0));
      error_y_total.push_back(total_error(1));
      error_z_total.push_back(total_error(2));

    }
    //Plot the results
    // plt::plot(defm_results(seqN(0,1),seqN(0,pt_num)),defm_results(seqN(1,1),seqN(0,pt_num)),defm_results(seqN(2,1),seqN(0,pt_num)));
    plt::figure_size(1000,800);
    plt::scatter(error_x_total,error_y_total,{{"c","black"}, {"marker","o"}});
    plt::title("Total deflection in XY plane");
    plt::xlabel("X (mm)");
    plt::ylabel("Y (mm)");
    
    plt::figure_size(1000,800);
    plt::scatter(error_x_total,error_z_total,{{"c","black"}, {"marker","o"}});
    plt::title("Total deflection in XY plane");
    plt::xlabel("X (mm)");
    plt::ylabel("Z (mm)");

    plt::figure_size(1000,800);
    plt::scatter(error_y_total,error_z_total,{{"c","black"}, {"marker","o"}});
    plt::title("Total deflection in XY plane");
    plt::xlabel("Y (mm)");
    plt::ylabel("Z (mm)");
    plt::show();
    return 0;
}