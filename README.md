# README #

A02L Robotic Arm Positioning Accuracy Improvement: Flexible Link/Joint Modeling

### Flexible link and joint modeling ###

* This model recalculate the kinematics of robotic arm due to flexiblity of link/joint under static and dynamic load
    * Static and dyanmic load is calculated using recursive newton-euler method.
    * FEA simulation data of each link is used to calculate the deformation matrix of 6DOF
    * The deformations caused by link and joint are around 1:3. Max deformation could be up to 6.5mm 
    * Plotted data shows EE deformation at various random poses.
* Version 1.0
* [Confluence page : flexible link/joint modeling](https://flexivrobotics.atlassian.net/wiki/spaces/AEAI/pages/2478047417/A02L+Robotic+Arm+Position+accuracy+study)

### Required library ###

* Eigen
* matplotlibcpp : https://github.com/lava/matplotlib-cpp

### How to run ###

* Sample testing code of single pose:
    ``` C++
    std::tie(links_error,joints_error) = kinematics_flex::getT2BaseFlexible(q_rad,qdot,qddot,input_payload,input_payload_com);

    std::cout<< "Current joint position(degree, in MDH frame): "<< std::endl;
    for (const double &q : q_deg)
    {
        std::cout << q << " ";
    }
    std::cout << std::endl;
    std::cout<< "EE deflection error (in flange frame): "<< std::endl;
    std::cout<< "link error (x,y,z in mm): "<<links_error.transpose()<<std::endl;
    std::cout<< "joint error (x,y,z in mm): "<<joints_error.transpose()<<std::endl;
    std::cout<< "total error (x,y,z in mm): "<<(joints_error+links_error).transpose()<<std::endl;
    ```
* Sample testing code of multiple poses
    ``` C++
    for(int i=0;i< pt_num;i++)
    {
      MatrixXd q_deg_mat = MatrixXd::Random(1,7)*180;//define random joint pose between -180-+180
      Map<MatrixXd>(q_deg.data(),q_deg_mat.rows(),q_deg_mat.cols()) = q_deg_mat;
      std::vector<double> q_rad = kinematics_basic::deg2rad(q_deg);
      std::tie(links_error,joints_error) = kinematics_flex::getT2BaseFlexible(q_rad,qdot,qddot,input_payload,input_payload_com);
      total_error = links_error+joints_error;
      error_x_total.push_back(total_error(0));
      error_y_total.push_back(total_error(1));
      error_z_total.push_back(total_error(2));
    }

    ```
### Results ###
* Single pose testing result at home pose: <br> 
![Home pose](results/home_pose_deflection.png)

* Multiple poses testing results at various random poses: <br> 
![error in XY plane](results/Figure_1_XY.png)
![error in XZ plane](results/Figure_2_XZ.png)
![error in YZ plane](results/Figure_3_YZ.png)

### Who do I talk to? ###

* Houzhu Ding, Senior Robotics Engineer, houzhu.ding@flexiv.com
* PD Department, Flexiv Inc.