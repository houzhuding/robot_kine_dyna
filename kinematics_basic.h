#ifndef KINEMATICS_BASIC_H_INCLUDED
#define KINEMATICS_BASIC_H_INCLUDED

#include <vector>
#include <Eigen/Dense>
using namespace Eigen;
#define pi 3.14159265358979323846 /* pi */

namespace kinematics_basic
{   
    // Homogeneous transformation matrix 
    Matrix4d transform_MDH(double theta, double alpha, double a_nom, double d_nom) 
    {
    double stheta = std::sin(theta);
    double ctheta = std::cos(theta);
    double salpha = std::sin(alpha);
    double calpha = std::cos(alpha);
    Matrix4d H;
    H << ctheta, -stheta, 0, a_nom, stheta * calpha, ctheta * calpha, -salpha,
        -salpha * d_nom, stheta * salpha, ctheta * salpha, calpha, calpha * d_nom,
        0, 0, 0, 1;
    // std::cout<<H<<std::endl;
    return H;
    }

    // Forward kinematics
    std::vector<Matrix4d> getT_MDH(std::vector<double> theta) // in radian
    {
        // MDH parameters
        std::vector<double> aph_nom = {0.,     pi / 2, -pi / 2, -pi / 2,
                                pi / 2, pi / 2, pi / 2};
        std::vector<double> a_nom = {0., 0., 0., 0.020, -0.020, 0, 0.110};
        std::vector<double> d_nom = {0.365, 0.065, 0.395, 0.055, 0.385, 0.100, 0.};
        std::vector<Matrix4d> T_MDH{};
        for (int i = 0; i < 7; i++) 
        {
            Matrix4d H = kinematics_basic::transform_MDH(theta[i], aph_nom[i], a_nom[i], d_nom[i]);
            T_MDH.push_back(H);
        }
        Matrix4d H_flange;
        H_flange <<1,0,0,0,0,1,0,0,0,0,1,0.136,0,0,0,1;
        T_MDH.push_back(H_flange);
        return T_MDH;
    }

    // T_DE: Coordinate systems: D and E (origin frame E in MDH along Z )
    std::vector<Matrix4d> getT_DE()
    {
      std::vector<Matrix4d> T_DE{};
      double z_MDH2E[8] = {-0.1535,0.0215,-0.1405,0.0245,-0.1405,-0.0205,0.0840,0.01975};// z offset between MDH and E frame, in meter
      // consider the link frame should be in the output flange of the joint, an offset should be considered

      for (int i = 0; i < 8; i++) 
      {
          Matrix4d T_DEi = Matrix4d::Identity();
          T_DEi(2,3) = z_MDH2E[i];
          T_DE.push_back(T_DEi);
      }
      return T_DE;
    } // end of T_DE 


    std::vector<double> deg2rad(std::vector<double> theta_deg)
    {
        std::vector<double> theta_rad = {0,0,0,0,0,0,0};
        for(int i=0;i<7;i++)
        {
            theta_rad[i]=theta_deg[i]*pi/180;
        }
        return theta_rad;
    }
}


#endif