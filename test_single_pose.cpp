/*
 * Copyright 2022 Flexiv Inc.
 * 
 * Demo of link ee deflection due to link and joint flexibility at single pose
 * 
 * @author: Houzhu Ding (houzhu.ding@flexiv.com) 
 *
 */
#include <iostream>
#include <vector>
#include <string>
#include <Eigen/Dense>
#include <tuple>
#include <kinematics_flex.h>

using namespace Eigen;

int main() {
    std::vector<std::string> msg {"A02L Robotic Arm Positioning Accuracy Improvement:","Flexible Link/Joint Modeling"};

    for (const std::string &word : msg) 
    {
      std::cout << word << " ";
    }
    std::cout << std::endl;

    // Define the pose of the robot
    std::vector<double> q_deg = {0,-40,0,90,0,130,0}; // home
    // std::vector<double> q_deg = {0,-90,0,0,0,90,0}; // horizontal
    // std::vector<double> q_deg = {0,0,0,0,0,0,0}; // vertical

    // Define the velocity and acceleration of each joint
    std::vector<double> qdot = {0,0,0,0,0,0,0,0};
    std::vector<double> qddot = {0,0,0,0,0,0,0,0};

    double input_payload = 0; //kg
    std::vector<double> input_payload_com = {0,0,0}; //meter
    
    Vector3d links_error{};
    Vector3d joints_error{};

    std::vector<double> q_rad = kinematics_basic::deg2rad(q_deg);

    std::tie(links_error,joints_error) = kinematics_flex::getT2BaseFlexible(q_rad,qdot,qddot,input_payload,input_payload_com);

    std::cout<< "Current joint position(degree, in MDH frame): "<< std::endl;
    for (const double &q : q_deg)
    {
        std::cout << q << " ";
    }
    std::cout << std::endl;
    std::cout<< "EE deflection error (in flange frame): "<< std::endl;
    std::cout<< "link error (x,y,z in mm): "<<links_error.transpose()<<std::endl;
    std::cout<< "joint error (x,y,z in mm): "<<joints_error.transpose()<<std::endl;
    std::cout<< "total error (x,y,z in mm): "<<(joints_error+links_error).transpose()<<std::endl;

    return 0;
}