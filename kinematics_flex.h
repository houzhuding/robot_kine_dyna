#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <tuple>
#include <Eigen/Dense>

#include <dynamics.h>
#include <kinematics_basic.h>

#define pi 3.14159265358979323846 /* pi */
using namespace Eigen;

namespace kinematics_flex
{
  /**
   * @brief Forward kinematics of robot with flexible link and joint
   *
   */
    std::vector<double> readCSVFile(std::string fileName)
    {
        std::vector<double> linkDefVec;
        std::ifstream linkDefmCSV(fileName);
        if (!linkDefmCSV.is_open())
            throw std::runtime_error("Could not open the file");
        std::string line,colname;
        double val;
        // Read data line by line
        while(std::getline(linkDefmCSV, line))
        {
            
            std::stringstream ss(line);
            int colIdx = 0;
            while(ss >> val)
            {
                linkDefVec.push_back(val);
                // If the next token is a comma, ignore it and move on
                if(ss.peek() == ',') ss.ignore();
                // Increment the column index
                colIdx++;
            }
        }
        // Close file
        linkDefmCSV.close();
        return linkDefVec;
    } // read csv

    std::vector<Matrix4d> getLinkDefmMat()
    {
        std::string fileName = "resources/Link-defm-mat-0-7.csv";
        std::vector<double> linkDefVec;
        std::vector<Matrix4d> C_links;

        linkDefVec =  kinematics_flex::readCSVFile(fileName);
        Map<Matrix<double,192,4,RowMajor>> linkDefMat(linkDefVec.data());
        for(int i=0;i<42;i++)
        {
            Matrix4d C_links_load = linkDefMat(seqN(4*i,4),seqN(0,4));
            C_links.push_back(C_links_load);
        }
        return C_links;
    } // Link deformation matrix 

    std::vector<Matrix4d> getJointDefmMat()
    {
        std::string fileName = "resources/Joint-defm-mat-0-7.csv";
        std::vector<double> jointDefVec;
        std::vector<Matrix4d> C_joints;

        jointDefVec =  kinematics_flex::readCSVFile(fileName);
        Map<Matrix<double,192,4,RowMajor>> jointDefMat(jointDefVec.data());
        for(int i=0;i<42;i++)
        {
            Matrix4d C_joints_load = jointDefMat(seqN(4*i,4),seqN(0,4));
            C_joints.push_back(C_joints_load);
        }
        return C_joints;
    } // Joint deformation matrix 

   
 

    // T_EF: deformation between each two links, caused by flexible link and flexible joint (bending)
    std::tuple<std::vector<Matrix4d>,std::vector<Matrix4d>> getT_EF(
        std::vector<double> q,
        std::vector<double> qdot,
        std::vector<double> qddot,
        double input_payload,
        std::vector<double> input_payload_com)

    {
        std::vector<Matrix4d> T_EF_links, T_EF_joints;

        Matrix4d I = Matrix4d::Identity(); 

        std::vector<Matrix4d> C_links = kinematics_flex::getLinkDefmMat();

        std::vector<Matrix4d> C_joints = kinematics_flex::getJointDefmMat();

        std::vector<VectorXd> dyna_loads = dynamics::NewtonEulerMethod(q,qdot,qddot,input_payload,input_payload_com);
        
        for (int i = 0; i < 8; i++) // 7 joints and 1 flange
        {
            VectorXd load = dyna_loads[7-i];
            // std::cout<<load.transpose()<<std::endl;
            Matrix4d T_EF_link_all  = Matrix4d::Identity();
            Matrix4d T_EF_joint_all = Matrix4d::Identity();
            Matrix4d C_link;
            Matrix4d C_joint;
            for(int j =0;j<6;j++)// Mx,My,Mz,Fx,Fy,Fz
            {     // Links and joints deflection matrix 
                if (i<7) // joint 0-7 is actual joints
                {
                    C_link = C_links[i*6+j];
                    C_joint = C_joints[i*6+j];
                }
                else // joint 8 is a virtual joint
                {
                    C_link = Matrix4d::Identity();
                    C_joint = Matrix4d::Identity();
                }
                Matrix4d T_EF_link_i = load(j)*(C_link - I) + I; // load x coeff is the deformation at 6DOF
                T_EF_link_all *= T_EF_link_i;

                Matrix4d T_EF_joint_i = load(j)*(C_joint - I) + I;
                T_EF_joint_all *= T_EF_joint_i;
            }
            T_EF_links.push_back(T_EF_link_all);
            T_EF_joints.push_back(T_EF_joint_all);
            // std::cout<<T_EF_link_all<<std::endl;
            // std::cout<<T_EF_joint_all<<std::endl;
        }
        return {T_EF_links,T_EF_joints};
        }// END of get_T_EF

    // Updated FK with T_basic, T_DE, T_EF
    std::tuple<Vector3d,Vector3d> getT2BaseFlexible(std::vector<double> q,std::vector<double> qdot,std::vector<double> qddot,double input_payload,
        std::vector<double> input_payload_com)
    {
        Matrix4d fk_links = Matrix4d::Identity();
        std::vector<Matrix4d> T_i2base_links{};
        T_i2base_links.push_back(fk_links);

        Matrix4d fk_joints = Matrix4d::Identity();
        std::vector<Matrix4d> T_i2base_joints{};
        T_i2base_joints.push_back(fk_joints);

        Matrix4d fk_ori = Matrix4d::Identity();
        std::vector<Matrix4d> T_i2base_ori{};
        T_i2base_ori.push_back(fk_ori);


        std::vector<Matrix4d> T_MDH =kinematics_basic::getT_MDH(q); // basic fkine
        std::vector<Matrix4d> T_DE = kinematics_basic::getT_DE();   // E frame is at the link-link interface
        std::vector<Matrix4d> T_EF_links{};
        std::vector<Matrix4d> T_EF_joints{};
        std::tie(T_EF_links,T_EF_joints) =  getT_EF(q,qdot,qddot,input_payload,input_payload_com);

        for (int i = 0; i < 8; i++) 
        {
            //flexibility caused by links
            Matrix4d T_cur2pre_links = T_MDH[i]*T_DE[i]*T_EF_links[i]*T_DE[i].inverse();
            fk_links = fk_links*T_cur2pre_links;
            T_i2base_links.push_back(fk_links);

            // flexiblilty caused by joints
            Matrix4d T_cur2pre_joints = T_MDH[i]*T_DE[i]*T_EF_joints[i]*T_DE[i].inverse();
            fk_joints = fk_joints*T_cur2pre_joints;
            T_i2base_joints.push_back(fk_joints);
            
            // No flexibility
            Matrix4d T_cur2pre_ori = T_MDH[i];
            fk_ori = fk_ori*T_cur2pre_ori;
            T_i2base_ori.push_back(fk_ori);
        }
        // Compute the deformation caused by flexible joints, links, and no flexibility
        Vector4d flange_origin;
        flange_origin << 0,0,0,1;
        Vector4d ee_pos_original = T_i2base_ori[8]*flange_origin*1000;
        Vector4d ee_pos_flex_joints = T_i2base_joints[8]*flange_origin*1000;
        Vector4d ee_pos_flex_links = T_i2base_links[8]*flange_origin*1000;

        // // The deformation vectors 
        // std::vector<Vector3d> defm_vec;

        // defm_vec.push_back(ee_pos_original.head<3>());
        // defm_vec.push_back(ee_pos_flex_joints.head<3>());
        // defm_vec.push_back(ee_pos_flex_links.head<3>());
        // return defm_vec;
        // Total deflection caused by link and joint
        Vector3d defm_error_link  = ee_pos_original.head<3>() - ee_pos_flex_links.head<3>();
        Vector3d defm_error_joint = ee_pos_original.head<3>() - ee_pos_flex_joints.head<3>();

        return {defm_error_link,defm_error_joint};

        }
}
