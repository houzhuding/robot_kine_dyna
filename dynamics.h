/*
 * Copyright 2022 Flexiv Inc.
 */
#include <Eigen/Dense>
#include <kinematics_basic.h>
#define pi 3.14159265358979323846 /* pi */
using namespace Eigen;

namespace dynamics
{
    std::vector<VectorXd> NewtonEulerMethod(
        std::vector<double> q,
        std::vector<double> qdot,
        std::vector<double> qddot,
        double input_payload,
        std::vector<double> input_payload_com)
    {   
        // DOF 
        int DOFs = 7;
        // Output
        std::vector<VectorXd> dyna_loads;

        // Kinematics
        std::vector<double> theta_rad = q;
        // get transformation matrix of T_E which defines load on the link to link interface
        std::vector<Matrix4d> T_MDHs = kinematics_basic::getT_MDH(theta_rad);
        std::vector<Matrix4d> T_DE = kinematics_basic::getT_DE();
        std::vector<Matrix4d> T_E = T_MDHs;
        for (int i=0;i<8;i++)
        {   
            if(i>0)
                T_E[i] = (T_DE[i-1].inverse()*T_MDHs[i]*T_DE[i]);
            else
                T_E[i] = (T_MDHs[i]*T_DE[i]);

            // T_E[i] = T_MDHs[i];
            // std::cout<<T_MDHs[i]-T_E[i]<<std::endl;
            // std::cout<<T_E[i]<<std::endl;
        }

        // Input payload info
        double payload = input_payload;
        std::vector<double> payload_com = input_payload_com;
        
        // COM and Mass properties of links and load
        MatrixXd COMs =  (MatrixXd(8,3) << 
                0,	   -0.006,	-0.061,
                0,	    0.098,	 0.001,
                0.012,	0.003,	-0.057,
                -0.013, -0.101,	 0.006,
                0,	   -0.003,	-0.057,
                0.069, -0.009,	-0.013,
                0,	    0,	     0.08,
                payload_com[0],payload_com[1],payload_com[2]).finished().transpose();

        RowVectorXd MASS =  (RowVectorXd(8) << 3.673,2.734,2.397,2.397,2.363,2.135,0.793,payload).finished();
        MatrixXd I    = (MatrixXd(7,9)<< 
        0.026944,	 0.000026,	 0.000018,
        0.000026,	 0.026501,	-0.001648,
        0.000018,	-0.001648,	 0.007466,

        0.022497,	0.000028,	0.000002,
        0.000028,	0.003491,	0.000246,
        0.000002,	0.000246,	0.023339,
                
        0.013139,	-0.000116,	 0.001341,
        -0.000116,	 0.012721,	-0.000645,
        0.001341,	-0.000645,	 0.003638,
                
        0.017283,	-0.001460,	-0.000106,
        -0.001460,	 0.002826,	 0.001124,
        -0.000106,	 0.001124,	 0.017649,
                
        0.012435,	 0.000000,	 0.000004,
        0.000000,	 0.011953,	-0.000722,
        0.000004,	-0.000722,	 0.003155,
                
        0.003429,	 0.000743,	-0.001636,
        0.000743,	 0.007707,	-0.000246,
        -0.001636,	-0.000246,	 0.008057,
                
        0.001156,	-0.000001,	 0.000002,
        -0.000001,	 0.001177,	-0.000003,
        0.000002,	-0.000003,	 0.000990).finished();

        Vector3d gravity = (Vector3d()<<0,0,-9.81).finished();

        MatrixXd w = MatrixXd::Zero(3,DOFs+1);
        MatrixXd wdot = MatrixXd::Zero(3,DOFs+1);
        // MatrixXd vdot = MatrixXd::Zero(3,DOFs+1);
        MatrixXd vcdot = MatrixXd::Zero(3,DOFs+1);
        Vector3d Z = (Vector3d()<<0,0,1).finished();
        
        // Initilization of w and wdot, and vdot (due to gravity)
        // First load is on joint 0, base link and link 1

        MatrixXd vdot = gravity.replicate(1,DOFs+1);

        Vector3d Pc0 = COMs(seqN(0,3),seqN(0,1));
        // convert the com from MDH to E frame
        Pc0(2) -= T_DE[0](2,3);

        Vector3d w_0 = qdot[0]*Z;
        w(seqN(0,3),seqN(0,1)) = w_0;

        Vector3d wdot_0 = qddot[0]*Z;
        wdot(seqN(0,3),seqN(0,1)) = wdot_0;

        Vector3d vdot_0  = vdot(seqN(0,3),seqN(0,1));

        Vector3d vcdot_0   = wdot_0.cross(Pc0)+w_0.cross(w_0.cross(Pc0)) +vdot_0;
        vcdot(seqN(0,3),seqN(0,1)) = vcdot_0;

        std::vector<Vector3d> F;
        std::vector<Vector3d> N;
        Vector3d F0 = MASS(0)*vcdot_0;
        F.push_back(F0);
        Vector3d N0 = Vector3d::Zero();
        N.push_back(N0);

        // std::cout << "vdot"<< vdot << std::endl;
        // std::cout <<  "w" <<  w << std::endl;
        // std::cout << "wdot"<< wdot_0 << std::endl;
        // std::cout << "vdot"<< vdot_0 << std::endl;

        //Outward iteration
        for(int i = 0;i<DOFs;i++)
        {  
            
            Matrix3d Ri = T_E[i+1](seqN(0,3),seqN(0,3)).transpose();
            Vector3d wi = w(seqN(0,3),seqN(i,1));
            Vector3d wdoti = wdot(seqN(0,3),seqN(i,1));
            Vector3d vdoti = vdot(seqN(0,3),seqN(i,1));
            Vector3d vcdoti = vcdot(seqN(0,3),seqN(i,1));
            MatrixXd Ii_1 = I(seqN(i,1),seqN(0,9));
            Ii_1.resize(3,3);
            // std::cout<<Ii_1<<std::endl;
            // std::cout<<wi.cross(Vector3d(Ii_1*wi))<<std::endl;

            Vector3d Pi_1 = T_E[i+1](seqN(0,3),3);
            Vector3d Pci_1 = COMs(seqN(0,3),seqN(i+1,1));
            Pci_1(2) -= T_DE[i+1](2,3);

            Vector3d wi_1 = Ri*wi + qdot[i+1]*Z;
            w(seqN(0,3),seqN(i+1,1)) = wi_1;

            Vector3d wdoti_1 = Ri*wdoti+Ri*wi.cross(qdot[i]*Z)+qddot[i+1]*Z;
            wdot(seqN(0,3),seqN(i+1,1)) = wdoti_1;
            
            Vector3d vdoti_1 = Ri*(wdoti.cross(Pi_1)+wi.cross(wi.cross(Pi_1)) + vdoti);
            vdot(seqN(0,3),seqN(i+1,1)) = vdoti_1;
            
            Vector3d vcdoti_1   = wdoti_1.cross(Pci_1)+wi_1.cross(wi_1.cross(Pci_1)) + vdoti_1;
            vcdot(seqN(0,3),seqN(i+1,1)) = vcdoti_1;

            // std::cout << Pci_1 << std::endl;

            // std::cout << wi_1 << std::endl;
            // std::cout << wdoti_1 << std::endl;
            // std::cout << vdoti_1 << std::endl;

            Vector3d Fi_1 = MASS(i+1)*vcdoti_1;
            // std::cout << i << " Fi_1"<< std::endl;
            // std::cout << vcdoti_1 << std::endl;
            // std::cout << Fi_1 << std::endl;
            Vector3d Ni_1 = Ii_1*wdoti_1 + wi_1.cross(Vector3d(Ii_1*wi_1));
            F.push_back(Fi_1);
            N.push_back(Ni_1); 
        }   
        // Inward Iteration
        Vector3d f_ext;f_ext<<0,0,0;
        Vector3d n_ext;n_ext<<0,0,0;
        std::vector<Vector3d> f;
        // f.push_back(f_ext);
        std::vector<Vector3d> n;
        // n.push_back(n_ext);
        for(int i = DOFs;i>-1;i--)
        {  
            Matrix3d R1_i;
            Vector3d Fi = F[i];
            Vector3d Ni = N[i];
            Vector3d Pi_1;
            Vector3d fi_1;
            Vector3d ni_1;

            if (i == DOFs)
            {
                fi_1 = f_ext;
                ni_1 = n_ext;
                R1_i = Matrix3d::Identity();
                Pi_1 = (Vector3d()<< 0,0,0).finished();//The external force origin defined in flange frame
                
            }
            else
            {
                fi_1 = f.back();
                ni_1 = n.back();
                R1_i = T_E[i+1](seqN(0,3),seqN(0,3));
                Pi_1 = T_E[i+1](seqN(0,3),3);//% The vector of flange frame in MDH7, the vector of MDHi+1 in MDHi, MDH 1 in MDH 0 is not included.
            }

            Vector3d Pci = COMs(seqN(0,3),seqN(i,1));
            Pci(2) -= T_DE[i](2,3);
            
            Vector3d fi = R1_i*fi_1+Fi;
            f.push_back(fi);

            Vector3d ni = Ni+R1_i*ni_1+Pci.cross(Fi)+Pi_1.cross(R1_i*fi_1);
            n.push_back(ni);

            VectorXd dyna_load(6);
            dyna_load << ni, fi;

            dyna_loads.push_back(dyna_load);
            // std::cout<< i << "  M xyz" << std::endl;
            // std::cout<<ni.transpose()<<std::endl;
            // std::cout<< i << "  F xyz" << std::endl;
            // std::cout<<fi.transpose()<<std::endl;
            
        }
        return dyna_loads;
    }
}
